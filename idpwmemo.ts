/* IDPWMemo
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

class MTRandom {
	private readonly mt = new MersenneTwister();

	public constructor() {}

	public setSeed(seed: Uint32Array): void {
		this.mt.init_by_array(seed);
	}

	public nextInt(): number {
		return this.mt.genrand_int32();
	}
}

class IDPWMemoCryptor {
	private static readonly VERSION = 2;
	private static readonly MAX_BLOCKSIZE = Math.min(1024, UnkoCrypto.MAX_BLOCKSIZE);
	private static readonly encoder = new TextEncoder();
	private readonly rand = new MTRandom();
	private readonly cs = new CRC32();

	public constructor() {}

	public static getBytes(s: string): Int8Array {
		return new Int8Array(IDPWMemoCryptor.encoder.encode(s).buffer);
	}

	private static initGenSeed(size: number): Uint32Array {
		const seed = new Uint32Array(size);
		seed[0] = 0x98765432;
		seed[1] = 0xF1E2D3C4;
		for (let i = 2; i < seed.length; i++) {
			seed[i] = seed[i - 2] ^ (seed[i - 1] >>> ((i - 1) & 0xF)) ^ (seed[i - 1] << ((i + i + 1) & 0xF));
		}
		return seed;
	}

	private static genSeedV1(password: Int8Array): Uint32Array {
		if (password.length === 0) {
			return IDPWMemoCryptor.initGenSeed(23);
		}
		const seed = IDPWMemoCryptor.initGenSeed(password.length + 13);
		let p = 0;
		for (let i = 0; i < seed.length; i++) {
			for (let j = 0; j < 4; j++) {
				seed[i] |= password[p] << (((j + i * i) & 3) << 3);
				p++;
				if (p >= password.length) {
					p = 0;
				}
			}
		}
		return seed;
	}

	private static encryptBlockSize(srclen: number): number {
		const MAX_BLOCKSIZE = IDPWMemoCryptor.MAX_BLOCKSIZE;
		const MIN_BLOCKSIZE = UnkoCrypto.MIN_BLOCKSIZE;
		const META_SIZE = UnkoCrypto.META_SIZE;
		let size = Math.max(MIN_BLOCKSIZE, srclen + META_SIZE);
		if (size <= MAX_BLOCKSIZE) {
			return size;
		}
		size = MIN_BLOCKSIZE;
		let blockCount = Math.floor((srclen + (size - META_SIZE) - 1) / (size - META_SIZE));
		let totalSize = size * blockCount;
		for (let sz = MIN_BLOCKSIZE + 1; sz <= MAX_BLOCKSIZE; sz++) {
			blockCount = Math.floor((srclen + (sz - META_SIZE) - 1) / (sz - META_SIZE));
			if (sz * blockCount < totalSize) {
				size = sz;
				totalSize = sz * blockCount;
			}
		}
		return size;
	}

	public decryptV1(password: string|Int8Array, src: ArrayBufferLike|ArrayBufferView): ArrayBuffer|null {
		if (typeof password === 'string') {
			password = IDPWMemoCryptor.getBytes(password);
		}
		const MAX_BLOCKSIZE = IDPWMemoCryptor.MAX_BLOCKSIZE;
		const rand = this.rand;
		const cs = this.cs;
		const out = new ArrayBuffer(src.byteLength);
		const seed = IDPWMemoCryptor.genSeedV1(password);
		for (let size = Math.min(src.byteLength, MAX_BLOCKSIZE); size >= UnkoCrypto.MIN_BLOCKSIZE; size--) {
			if (src.byteLength % size !== 0) {
				continue;
			}
			rand.setSeed(seed);
			try {
				const len = UnkoCrypto.decrypt(size, cs, rand, src, out);
				return out.slice(0, len);
			} catch {
				// continue;
			}
		}
		return null;
	}

	public encryptV1(password: string|Int8Array, src: ArrayBufferLike|ArrayBufferView): ArrayBuffer {
		if (typeof password === 'string') {
			password = IDPWMemoCryptor.getBytes(password);
		}
		const blockSize = IDPWMemoCryptor.encryptBlockSize(src.byteLength);
		const dataSize = blockSize - UnkoCrypto.META_SIZE;
		const blockCount = Math.max(1, Math.ceil(src.byteLength / dataSize));
		const out = new ArrayBuffer(blockCount * blockSize);
		this.rand.setSeed(
			IDPWMemoCryptor.genSeedV1(password)
		);
		const len = UnkoCrypto.encrypt(
			blockSize,
			this.cs,
			this.rand,
			src,
			out
		);
		if (out.byteLength !== len) {
			return out.slice(0, len);
		}
		return out;
	}

	public decryptRepeatV1(n: number, password: string|Int8Array, src: ArrayBufferLike|ArrayBufferView): ArrayBuffer|null {
		let buf: ArrayBufferLike|ArrayBufferView|null = src;
		for (let i = 0; i < n; i++) {
			if (buf === null) {
				return null;
			}
			buf = this.decryptV1(password, buf);
		}
		if (buf instanceof ArrayBuffer) {
			return buf;
		}
		return null;
	}

	public encryptRepeatV1(n: number, password: string|Int8Array, src: ArrayBufferLike|ArrayBufferView): ArrayBuffer {
		let buf: ArrayBufferLike|ArrayBufferView = src;
		for (let i = 0; i < n; i++) {
			buf = this.encryptV1(password, buf);
		}
		if (buf instanceof ArrayBuffer) {
			return buf
		}
		throw '[BUG] REQUIRE: n > 0';
	}

	public static checkSrcType(src: ArrayBufferLike|ArrayBufferView): number {
		const MAX_BLOCKSIZE = IDPWMemoCryptor.MAX_BLOCKSIZE;
		const MIN_BLOCKSIZE = UnkoCrypto.MIN_BLOCKSIZE;
		const META_SIZE = UnkoCrypto.META_SIZE;
		let srcType = 0;
		const view = ArrayBuffer.isView(src)
				   ? new Uint8Array(src.buffer, src.byteOffset, src.byteLength)
				   : new Uint8Array(src);
		let version = 0;
		for (let i = 0; i < 8; i++) {
			version ^= view[i];
		}
		if (version === IDPWMemoCryptor.VERSION) {
			let dataLen = src.byteLength-1;
			for (let size = Math.min(dataLen, MAX_BLOCKSIZE); size >= MIN_BLOCKSIZE; size--) {
				if (dataLen % size === 0) {
					srcType = 2;
					break;
				}
			}
		}
		for (let size = Math.min(src.byteLength, MAX_BLOCKSIZE); size >= MIN_BLOCKSIZE; size--) {
			if (src.byteLength % size === 0) {
				srcType |= 1;
				break;
			}
		}
		return srcType;
	}

	private static genSeedV2(password: ArrayBufferLike|ArrayBufferView): Uint32Array {
		if (password.byteLength === 0) {
			return IDPWMemoCryptor.initGenSeed(37);
		}
		const seed = IDPWMemoCryptor.initGenSeed(password.byteLength + 29);
		const view = ArrayBuffer.isView(password)
				   ? new Uint8Array(password.buffer, password.byteOffset, password.byteLength)
				   : new Uint8Array(password);
		let p = 0;
		for (let i = 0; i < seed.length; i++) {
			for (let j = 0; j < 4; j++) {
				seed[i] ^= (0xFF & view[p]) << (((j + i * i) & 3) << 3);
				p++;
				if (p >= view.length) {
					p = 0;
				}
			}
		}
		return seed;
	}

	public decryptV2(password: string|ArrayBufferLike|ArrayBufferView, src: ArrayBufferLike|ArrayBufferView): ArrayBuffer|null {
		if (typeof password === 'string') {
			password = IDPWMemoCryptor.getBytes(password);
		}
		const MAX_BLOCKSIZE = IDPWMemoCryptor.MAX_BLOCKSIZE;
		const rand = this.rand;
		const cs = this.cs;
		const src1 = ArrayBuffer.isView(src)
				   ? new Uint8Array(src.buffer, src.byteOffset+1, src.byteLength-1)
				   : new Uint8Array(src, 1);
		const out1 = new ArrayBuffer(src1.byteLength);
		const out2 = new ArrayBuffer(src1.byteLength);
		const seed = IDPWMemoCryptor.genSeedV2(password);
		for (let size1 = Math.min(src1.byteLength, MAX_BLOCKSIZE); size1 >= UnkoCrypto.MIN_BLOCKSIZE; size1--) {
			if (src1.byteLength % size1 !== 0) {
				continue;
			}
			rand.setSeed(seed);
			try {
				const len1 = UnkoCrypto.decrypt(size1, cs, rand, src1, out1);
				const src2 = new Uint8Array(out1, 0, len1);
				for (let size2 = Math.min(src2.byteLength, MAX_BLOCKSIZE); size2 >= UnkoCrypto.MIN_BLOCKSIZE; size2--) {
					if (src2.byteLength % size2 !== 0) {
						continue;
					}
					rand.setSeed(seed);
					try {
						const len2 = UnkoCrypto.decrypt(size2, cs, rand, src2, out2);
						return out2.slice(0, len2);
					} catch {
						// continue;
					}
				}
			} catch {
				// continue;
			}
		}
		return null;
	}

	public encryptV2(password: string|ArrayBufferLike|ArrayBufferView, src: ArrayBufferLike|ArrayBufferView): ArrayBuffer {
		if (typeof password === 'string') {
			password = IDPWMemoCryptor.getBytes(password);
		}
		let blockSize = IDPWMemoCryptor.encryptBlockSize(src.byteLength);
		let dataSize = blockSize - UnkoCrypto.META_SIZE;
		let blockCount = Math.max(1, Math.ceil(src.byteLength / dataSize));
		let out = new ArrayBuffer(blockCount * blockSize);
		const seed = IDPWMemoCryptor.genSeedV2(password);
		this.rand.setSeed(seed);
		let len = UnkoCrypto.encrypt(blockSize, this.cs, this.rand, src, out);
		const tmpSrc = out.slice(0, len);
		blockSize = IDPWMemoCryptor.encryptBlockSize(tmpSrc.byteLength);
		dataSize = blockSize - UnkoCrypto.META_SIZE;
		blockCount = Math.max(1, Math.ceil(tmpSrc.byteLength / dataSize));
		out = new ArrayBuffer(blockCount * blockSize + 1);
		const tmpOut = new Uint8Array(out, 1);
		this.rand.setSeed(seed);
		len = UnkoCrypto.encrypt(blockSize, this.cs, this.rand, tmpSrc, tmpOut);
		const ret = len + 1 == out.byteLength
				  ? out
				  : out.slice(0, len + 1);
		const view = new Uint8Array(ret);
		view[0] = IDPWMemoCryptor.VERSION;
		for (let i = 1; i < 8; i++) {
			view[0] ^= view[i];
		}
		return ret;
	}
}

class IDPWMemoItemValue {
	public static readonly SERVICE_NAME      = 0;
	public static readonly SERVICE_URL       = 1;
	public static readonly ID                = 2;
	public static readonly PASSWORD          = 3;
	public static readonly EMAIL             = 4;
	public static readonly REMINDER_QUESTION = 5;
	public static readonly REMINDER_ANSWER   = 6;
	public static readonly DESCTIPTION       = 7;

	public static typeName(valueType: number): string {
		switch (valueType) {
		case IDPWMemoItemValue.SERVICE_NAME:
			return 'service name';
		case IDPWMemoItemValue.SERVICE_URL:
			return 'service url';
		case IDPWMemoItemValue.ID:
			return 'id';
		case IDPWMemoItemValue.PASSWORD:
			return 'password';
		case IDPWMemoItemValue.EMAIL:
			return 'email';
		case IDPWMemoItemValue.REMINDER_QUESTION:
			return 'reminder question';
		case IDPWMemoItemValue.REMINDER_ANSWER:
			return 'reminder answer';
		case IDPWMemoItemValue.DESCTIPTION:
			return 'description';
		default:
			return 'unknown';
		}
	}

	public valueType: number;
	public value: string;

	public constructor(valueType: number, value: string) {
		this.valueType = valueType;
		this.value = value;
	}

	public set(valueType: number, value: string): void {
		this.valueType = valueType;
		this.value = value;
	}

	public getClone(): IDPWMemoItemValue {
		return new IDPWMemoItemValue(this.valueType, this.value);
	}

	public getTypeName(): string {
		return IDPWMemoItemValue.typeName(this.valueType);
	}

	public static load(src: DataInput): IDPWMemoItemValue {
		const valueType = src.readByte();
		const value = src.readUTF();
		return new IDPWMemoItemValue(valueType, value);
	}

	public save(dst: DataOutput): void {
		dst.writeByte(this.valueType);
		dst.writeUTF(this.value);
	}

	public sizeHint(): number {
		return 1 + DataOutput.sizeHintWriteUTF(this.value);
	}
}

class IDPWMemoItemService {
	public values: IDPWMemoItemValue[];
	public secrets: ArrayBuffer;
	public time: bigint;

	public constructor(values: IDPWMemoItemValue[], secrets: ArrayBuffer, time: bigint) {
		this.values = values;
		this.secrets = secrets;
		this.time = time;
	}

	public static create(serviceName: string): IDPWMemoItemService {
		const value = new IDPWMemoItemValue(
			IDPWMemoItemValue.SERVICE_NAME,
			serviceName
		);
		return new IDPWMemoItemService([value], new ArrayBuffer(0), 0n);
	}

	public getServiceName(): string {
		for (const value of this.values) {
			if (value.valueType === IDPWMemoItemValue.SERVICE_NAME) {
				return value.value;
			}
		}
		return '';
	}

	public static readSecrets(src: DataInput): IDPWMemoItemValue[] {
		const values: IDPWMemoItemValue[] = [];
		const vlen = src.readInt();
		for (let i = 0; i < vlen; i++) {
			values.push(IDPWMemoItemValue.load(src));
		}
		return values;
	}

	public static writeSecrets(dst: DataOutput, values: IDPWMemoItemValue[]): void {
		dst.writeInt(values.length);
		for (const value of values) {
			value.save(dst);
		}
	}

	public static loadV1(src: DataInput): IDPWMemoItemService {
		const values: IDPWMemoItemValue[] = [];
		const vlen = src.readInt();
		for (let i = 0; i < vlen; i++) {
			values.push(IDPWMemoItemValue.load(src));
		}
		const slen = src.readInt();
		const secrets = new ArrayBuffer(slen);
		src.readFully(secrets);
		return new IDPWMemoItemService(values, secrets, 0n);
	}

	public static load(src: DataInput): IDPWMemoItemService {
		const time = src.readLong();
		const values: IDPWMemoItemValue[] = [];
		const vlen = src.readInt();
		for (let i = 0; i < vlen; i++) {
			values.push(IDPWMemoItemValue.load(src));
		}
		const slen = src.readInt();
		const secrets = new ArrayBuffer(slen);
		src.readFully(secrets);
		return new IDPWMemoItemService(values, secrets, time);
	}

	public save(dst: DataOutput): void {
		dst.writeLong(this.time);
		dst.writeInt(this.values.length);
		for (const value of this.values) {
			value.save(dst);
		}
		dst.writeInt(this.secrets.byteLength);
		dst.write(this.secrets);
	}

	public sizeHint(): number {
		let size = 8+4+4+this.secrets.byteLength;
		for (const value of this.values) {
			size += value.sizeHint();
		}
		return size;
	}
}

class IDPWMemoItemMemo {
	public static readonly VERSION = 2;
	private services: IDPWMemoItemService[];

	public constructor(services?: IDPWMemoItemService[]) {
		this.services = services ?? [];
	}

	public getServiceCount(): number {
		return this.services.length;
	}

	public addService(newService: IDPWMemoItemService): number {
		this.services.push(newService);
		return this.services.length - 1;
	}

	public getService(index: number): IDPWMemoItemService {
		return this.services[index];
	}

	public setService(index: number, service: IDPWMemoItemService): void {
		this.services[index] = service;
	}

	public removeService(index: number): void {
		this.services.splice(index, 1);
	}

	public static load(src: DataInput): IDPWMemoItemMemo {
		const version = src.readInt();
		if (version < 0 || version > IDPWMemoItemMemo.VERSION) {
			throw `INVALID MEMO VERSION (${version})`;
		}
		const count = src.readInt();
		const services: IDPWMemoItemService[] = [];
		if (version === 1) {
			for (let i = 0; i < count; i++) {
				services.push(IDPWMemoItemService.loadV1(src));
			}
		} else {
			for (let i = 0; i < count; i++) {
				services.push(IDPWMemoItemService.load(src));
			}
		}
		return new IDPWMemoItemMemo(services);
	}

	public save(dst: DataOutput): void {
		dst.writeInt(IDPWMemoItemMemo.VERSION);
		dst.writeInt(this.services.length);
		for (const service of this.services) {
			service.save(dst);
		}
	}

	public sizeHint(): number {
		let size = 4+4;
		for (const service of this.services) {
			size += service.sizeHint();
		}
		return size;
	}

	public trim(): IDPWMemoItemMemo {
		const services: IDPWMemoItemService[] = [];
		for (let i = 0; i < this.getServiceCount(); i++) {
			const s = this.getService(i);
			if (s.getServiceName().trim() === '') {
				continue;
			}
			const values: IDPWMemoItemValue[] = [];
			for (const v of s.values) {
				if (v.value.trim() !== '') {
					values.push(v);
				}
			}
			const ns = new IDPWMemoItemService(values, s.secrets, s.time);
			services.push(ns);
		}
		return new IDPWMemoItemMemo(services);
	}
}

type IDPWMemoConverter = (buf: ArrayBufferLike|ArrayBufferView) => (ArrayBuffer|null);

class IDPWMemo {
	private cryptor = new IDPWMemoCryptor();
	private version: number = 0;

	private encryptV1: IDPWMemoConverter|null = null;
	private decryptV1: IDPWMemoConverter|null = null;
	private encryptV2: IDPWMemoConverter|null = null;
	private decryptV2: IDPWMemoConverter|null = null;

	private memo: IDPWMemoItemMemo|null = null;
	private secrets: IDPWMemoItemValue[]|null = null;
	private selectedService: IDPWMemoItemService|null = null;
	private selectedIndex: number = 0;
	private serviceNames: string[]|null = null;

	public constructor() {}

	public hasMemo(): boolean {
		return this.memo !== null;
	}

	public setPassword(password: string): void {
		const cryptor = this.cryptor;
		const passBuf = IDPWMemoCryptor.getBytes(password).buffer;
		const raw = cryptor.encryptV1('', passBuf);
		const keyV2 = cryptor.encryptV2(passBuf, passBuf);
		const getKey = () => new Int8Array(cryptor.decryptV1('', raw)!);
		this.encryptV1 = buf => {
			const key = getKey();
			return cryptor.encryptRepeatV1(2, key, buf);
		};
		this.decryptV1 = buf => {
			const key = getKey();
			return cryptor.decryptRepeatV1(2, key, buf);
		};
		this.encryptV2 = buf => cryptor.encryptV2(keyV2, buf);
		this.decryptV2 = buf => cryptor.decryptV2(keyV2, buf);
		this.memo = null;
		this.serviceNames = null;
		this.secrets = null;
		this.selectedService = null;
		this.selectedIndex = 0;
		this.version = 0;
	}

	public getServiceCount(): number {
		return this.memo?.getServiceCount() ?? 0;
	}

	public getServiceNames(): string[] {
		return this.serviceNames ?? []
	}

	public getSelectedServiceIndex(): number {
		if (this.selectedService === null) {
			return -1;
		}
		return this.selectedIndex;
	}

	public getService(index: number): IDPWMemoItemService|null {
		if (this.memo === null) {
			return null;
		}
		return this.memo.getService(index);
	}

	public getSelectedService(): IDPWMemoItemService|null {
		return this.selectedService;
	}

	public addNewService(name: string): number {
		if (this.memo === null) {
			return -1;
		}
		if (this.serviceNames === null) {
			return -1;
		}
		if (name.trim() === '') {
			return -1;
		}
		const s = IDPWMemoItemService.create(name);
		this.serviceNames.push(name);
		return this.memo.addService(s);
	}

	public addNewValue(valueType: number, value: string): IDPWMemoItemValue|null {
		if (this.selectedService === null) {
			return null;
		}
		const v = new IDPWMemoItemValue(valueType, value);
		this.selectedService.values.push(v);
		return v;
	}

	public addNewSecret(valueType: number, value: string): IDPWMemoItemValue|null {
		if (this.selectedService === null) {
			return null;
		}
		if (this.secrets === null) {
			if (this.selectedService.secrets.byteLength > 0) {
				return null;
			}
			this.secrets = [];
		}
		const v = new IDPWMemoItemValue(valueType, value);
		this.secrets.push(v);
		return v;
	}

	public getValues(): IDPWMemoItemValue[] {
		return this.selectedService?.values ?? [];
	}

	public getSecrets(): IDPWMemoItemValue[] {
		return this.secrets ?? [];
	}

	public getSelectedServiceName(): string {
		if (this.serviceNames === null) {
			return '';
		}
		if (this.selectedService === null) {
			return '';
		}
		return this.serviceNames[this.selectedIndex];
	}

	public selectService(index: number): boolean {
		if (this.memo === null) {
			return false;
		}
		if (index >= this.memo.getServiceCount()) {
			return false;
		}
		this.secrets = null;
		this.selectedService = this.memo.getService(index);
		this.selectedIndex = index;
		return true;
	}

	public setTime(time: number) {
		if (this.selectedService !== null) {
			this.selectedService.time = BigInt(time);
		}
	}

	public makeNewMemo(): boolean {
		if (this.decryptV1 === null) {
			return false;
		}
		this.secrets = null;
		this.selectedService = null;
		this.selectedIndex = 0;
		this.memo = new IDPWMemoItemMemo([]);
		this.serviceNames = [];
		this.version = 2;
		return true;
	}

	public loadMemo(buf: ArrayBufferLike|ArrayBufferView): boolean {
		if (this.decryptV1 === null) {
			return false;
		}
		if (this.decryptV2 === null) {
			return false;
		}
		const srcType = IDPWMemoCryptor.checkSrcType(buf);
		if (srcType === 0) {
			return false;
		}
		this.secrets = null;
		this.selectedService = null;
		this.selectedIndex = 0;
		let src: ArrayBuffer|null = null;
		if ((srcType & 2) !== 0) {
			src = this.decryptV2(buf);
			if (src !== null) {
				this.version = 2;
			}
		}
		if (src === null && ((srcType & 1) !== 0)) {
			src = this.decryptV1(buf)
			if (src !== null) {
				this.version = 1;
			}
		}
		if (src === null) {
			this.memo = null;
			this.serviceNames = null;
			this.version = 0;
			return false;
		}
		this.memo = IDPWMemoItemMemo.load(new DataInput(src));
		this.serviceNames = [];
		for (let i = 0; i < this.memo.getServiceCount(); i++) {
			this.serviceNames.push(
				this.memo.getService(i).getServiceName()
			);
		}
		return true;
	}

	public loadSecrets(): boolean {
		if (this.decryptV1 === null) {
			return false;
		}
		if (this.decryptV2 === null) {
			return false;
		}
		if (this.memo === null) {
			return false;
		}
		if (this.selectedService === null) {
			return false;
		}
		const buf = this.selectedService.secrets;
		if (buf.byteLength === 0) {
			this.secrets = null;
			return true;
		}
		const src = this.version === 1
				  ? this.decryptV1(buf)
				  : this.decryptV2(buf);
		if (src === null) {
			this.secrets = null;
			return false;
		}
		this.secrets = IDPWMemoItemService.readSecrets(new DataInput(src));
		return true;
	}

	public saveSecrets(): boolean {
		if (this.encryptV1 === null) {
			return false;
		}
		if (this.encryptV2 === null) {
			return false;
		}
		if (this.selectedService === null) {
			return false;
		}
		if (this.secrets === null) {
			return false;
		}
		const secrets: IDPWMemoItemValue[] = [];
		let size = 0;
		for (const v of this.secrets) {
			if (v.value.trim() !== '') {
				size += v.sizeHint();
				secrets.push(v);
			}
		}
		if (size === 0) {
			this.selectedService.secrets = new ArrayBuffer(0);
			return true;
		}
		const buf = new ArrayBuffer(size+4);
		IDPWMemoItemService.writeSecrets(new DataOutput(buf), secrets);
		const enc = this.version === 1
				  ? this.encryptV1(buf)
				  : this.encryptV2(buf);
		if (enc === null) {
			return false;
		}
		this.selectedService.secrets = enc;
		return true;
	}

	public saveMemo(): ArrayBuffer|null {
		if (this.encryptV1 === null) {
			return null;
		}
		if (this.encryptV2 === null) {
			return null;
		}
		if (this.memo === null) {
			return null;
		}
		const memo = this.memo.trim();
		const size = memo.sizeHint();
		const buf = new ArrayBuffer(size);
		memo.save(new DataOutput(buf));
		return this.version === 1
			 ? this.encryptV1(buf)
			 : this.encryptV2(buf);
	}
}
