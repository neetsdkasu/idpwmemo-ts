IDPWMemo-TS
===========


Javaで作った[IDPWMemo](https://github.com/neetsdkasu/unkocrypto/tree/master/IDPWMemo)のメモデータをブラウザで閲覧できるようにするためのアプリがこのIDPWMemo-TSである  




開発環境  
------------------------
OS: Windows7 SP1 Starter  
ブラウザ: Vivaldi 3.5.2115.81 (Stable 32-bit) (JavaScript V8 8.7.220.29)  
TypeScript: 3.8.3 (node.js v8.11.2, npm 6.14.4) (Target ES2020)    


