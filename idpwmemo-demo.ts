/* IDPWMemo Demo
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

const idpwMemo = new IDPWMemo();
let fileName = 'download.memo';

function makeNewMemo(): void {
	const p = <HTMLInputElement>document.querySelector('aside input.newpassword');
	idpwMemo.setPassword(p.value);
	idpwMemo.makeNewMemo();
	fileName = 'download.memo';
	document.querySelector('article > h3')!
		.textContent = 'memo *NEW*'
}

function setMemo(buf: ArrayBuffer, p: string): void {
	idpwMemo.setPassword(p);
	if (!idpwMemo.loadMemo(buf)) {
		throw 'failed to load';
	}
	const sel = document.querySelector('article select')!;
	const names = idpwMemo.getServiceNames();
	for (let i = 0; i < names.length; i++) {
		const op = sel.appendChild(
			document.createElement('option')
		);
		op.value = `${i}`;
		op.textContent = names[i];
	}
	document.querySelector('article > h3')!
		.textContent = `memo ( ${fileName} )`;
}

function showSampleMemo(): void {
	fileName = 'sample.memo';
	fetch('sample.memo')
	.then( res => res.blob() )
	.then( blob => blob.arrayBuffer() )
	.then( buf => setMemo(buf, 'さんぷるdayo') )
	.catch( err => alert(`${err}`) );
}

function showLocalMemo(): void {
	const f = <HTMLInputElement>document.querySelector('aside input[type="file"]');
	const p = <HTMLInputElement>document.querySelector('aside div + div input.password');
	if (f.files === null || f.files.length === 0) {
		alert('file?');
		return;
	}
	fileName = f.files[0].name;
	f.files[0].arrayBuffer()
	.then( buf => setMemo(buf, p.value) )
	.catch( err => alert(`${err}`) );
}

function showMemo(): void {
	const sp = document.querySelector('article span.download');
	if (sp !== null) {
		sp.parentNode!.removeChild(sp);
	}
	document.querySelector('article > h3')!.textContent = 'memo';
	document.querySelector('article select')!.innerHTML = '';
	document.querySelectorAll('article > fieldset div.values')
		.forEach( d => d.innerHTML = '');
	document.querySelectorAll('article > fieldset > legend')
		.forEach( e => {
			e.textContent = (e.textContent ?? '').split(' ')[0];
		});
	const r = <HTMLInputElement>document.querySelector('aside input[name="radio"]:checked');
	if (r.value === 'sample') {
		showSampleMemo();
	} else if (r.value === 'local') {
		showLocalMemo();
	} else {
		makeNewMemo();
	}
}

function addNewService(): void {
	if (!idpwMemo.hasMemo()) {
		return;
	}
	let name = window.prompt('service name?', '');
	name = (name ?? '').trim();
	if (name === '') {
		alert('canceled');
		return;
	}
	const index = idpwMemo.addNewService(name);
	const sel = document.querySelector('article select')!;
	const op = sel.appendChild(
		document.createElement('option')
	);
	op.value = `${index}`;
	op.textContent = name;
	op.selected = true;
	loadService();
}

function addNewValue(): void {
	const v = idpwMemo.addNewValue(IDPWMemoItemValue.ID, '');
	if (v !== null) {
		const divs = document.querySelectorAll('article > fieldset div.values');
		divs[0].appendChild(makeValueContent(v));
	}
}

function addNewSecret(): void {
	const v = idpwMemo.addNewSecret(IDPWMemoItemValue.PASSWORD, '');
	if (v !== null) {
		const divs = document.querySelectorAll('article > fieldset div.values');
		divs[1].appendChild(makeValueContent(v));
	}
}

function makeValueContent(v: IDPWMemoItemValue): HTMLElement {
	const div = document.createElement('div');
	const sel = div.appendChild(
		document.createElement('select')
	);
	for (let i = 0; i < 8; i++) {
		const op = sel.appendChild(
			document.createElement('option')
		);
		op.value = `${i}`;
		if (i === v.valueType) {
			op.selected = true;
		}
		op.textContent = IDPWMemoItemValue.typeName(i);
	}
	const tx = div.appendChild(
		document.createElement('input')
	);
	tx.type = 'text';
	tx.value = v.value;
	const update = () => {
		v.set(Number(sel.value), tx.value);
		const time = Date.now();
		idpwMemo.setTime(time);
		setUpdateTimeElement(time);
	};
	sel.addEventListener('change', () => update());
	tx.addEventListener('change', () => update());
	return div;
}

function setUpdateTimeElement(time: number): Element {
	let div = document.querySelector('div.lastupdate');
	if (div === null) {
		div = document.createElement('div');
		div.classList.add('lastupdate');
	}
	if (time === 0) {
		div.textContent = 'last update: ????/??/?? ??:??:??';
	} else {
		const d = new Date(time);
		div.textContent = `last update: ${d.toLocaleString()}`;
	}
	return div;
}

function loadService(): void {
	idpwMemo.saveSecrets();
	const sel = <HTMLSelectElement>document.querySelector('article select');
	if (sel.value === '') {
		return;
	}
	if (!idpwMemo.selectService(Number(sel.value))) {
		return;
	}
	const divs = document.querySelectorAll('article > fieldset div.values');
	divs.forEach( d => d.innerHTML = '');
	for (const v of idpwMemo.getValues()) {
		divs[0].appendChild(makeValueContent(v));
	}
	idpwMemo.loadSecrets();
	for (const v of idpwMemo.getSecrets()) {
		divs[1].appendChild(makeValueContent(v));
	}
	const time = idpwMemo.getSelectedService()!.time;
	divs[2].appendChild(setUpdateTimeElement(Number(time)));
	const name = idpwMemo.getSelectedServiceName();
	document.querySelectorAll('article > fieldset > legend')
		.forEach( e => {
			e.textContent =
				(e.textContent ?? '').split(' ')[0]
				+ ` ${name}`;
		});
}

function downloadMemo(): void {
	idpwMemo.saveSecrets();
	const buf = idpwMemo.saveMemo();
	if (buf === null) {
		return;
	}
	const size = buf.byteLength;
	if (fileName === '') {
		fileName = 'download.memo';
	}
	const reader = new FileReader();
	reader.addEventListener('load', () => {
		const url = reader.result;
		if (typeof url === 'string') {
			const b = document.querySelector('main > article button.download')!;
			const p = b.parentNode!;
			let sp = p.querySelector('span.download');
			if (sp === null) {
				sp = p.appendChild(document.createElement('span'));
				sp.classList.add('download');
				sp.appendChild(document.createTextNode(' '));
				sp.appendChild(document.createElement('a')).target = 'download';
				sp.appendChild(document.createElement('span'))
					.style.fontSize = 'small';
			}
			const a = <HTMLAnchorElement>sp.querySelector('a');
			a.href = url;
			a.download = fileName;
			a.textContent = fileName;
			sp.querySelector('span')!.textContent
				= ` (CREATION DATE: ${(new Date()).toLocaleString()}, SIZE: ${size} bytes)`;
		}
	});
	reader.readAsDataURL(new File([buf], fileName));
}

function IDPWMemoTest(): void {
	fetch('sample1.memo')
	.then( res => res.blob() )
	.then( blob => blob.arrayBuffer() )
	.then( buf => {
		console.log('version 1 file');
		const idpwmemo = new IDPWMemo();
		const p = 'さんぷるdayo';
		idpwmemo.setPassword(p);
		console.log(IDPWMemoCryptor.getBytes(p));
		console.log(`loading: ${idpwmemo.loadMemo(buf)}`);
		if (idpwmemo.hasMemo()) {
			const names = idpwmemo.getServiceNames();
			for (let i = 0; i < names.length; i++) {
				console.log(`[[${names[i]}]]`);
				idpwmemo.selectService(i);
				console.log(' [values]');
				for (const v of idpwmemo.getValues()) {
					console.log(`  ${v.getTypeName()}: ${v.value}`);
				}
				console.log(` [secrets] (${idpwmemo.loadSecrets()})`);
				for (const v of idpwmemo.getSecrets()) {
					console.log(`  ${v.getTypeName()}: ${v.value}`);
				}
				console.log(`  saveSecrets: ${idpwmemo.saveSecrets()}`);
			}
			const t = idpwmemo.saveMemo();
			if (t !== null) {
				console.log('=== Memo format version 2 ===');
				const idpwmemo2 = new IDPWMemo();
				idpwmemo2.setPassword(p);
				console.log(`loading: ${idpwmemo2.loadMemo(t)}`);
				if (idpwmemo2.hasMemo()) {
					const names = idpwmemo2.getServiceNames();
					for (let i = 0; i < names.length; i++) {
						console.log(`[[${names[i]}]]`);
						idpwmemo2.selectService(i);
						console.log(' [values]');
						for (const v of idpwmemo2.getValues()) {
							console.log(`  ${v.getTypeName()}: ${v.value}`);
						}
						console.log(` [secrets] (${idpwmemo2.loadSecrets()})`);
						for (const v of idpwmemo2.getSecrets()) {
							console.log(`  ${v.getTypeName()}: ${v.value}`);
						}
						console.log(`  saveSecrets: ${idpwmemo2.saveSecrets()}`);
					}
				}
			}
		}
	})
	.catch( err => {
		console.log(`failed: ${err}`);
	});

	fetch('sample.memo')
	.then( res => res.blob() )
	.then( blob => blob.arrayBuffer() )
	.then( buf => {
		console.log('version 2 file');
		const idpwmemo = new IDPWMemo();
		const p = 'さんぷるdayo';
		idpwmemo.setPassword(p);
		console.log(IDPWMemoCryptor.getBytes(p));
		console.log(`loading: ${idpwmemo.loadMemo(buf)}`);
		if (idpwmemo.hasMemo()) {
			const names = idpwmemo.getServiceNames();
			for (let i = 0; i < names.length; i++) {
				console.log(`[[${names[i]}]]`);
				idpwmemo.selectService(i);
				console.log(' [values]');
				for (const v of idpwmemo.getValues()) {
					console.log(`  ${v.getTypeName()}: ${v.value}`);
				}
				console.log(` [secrets] (${idpwmemo.loadSecrets()})`);
				for (const v of idpwmemo.getSecrets()) {
					console.log(`  ${v.getTypeName()}: ${v.value}`);
				}
				console.log(`  saveSecrets: ${idpwmemo.saveSecrets()}`);
			}
			const t = idpwmemo.saveMemo();
			if (t !== null) {
				const a = new Uint8Array(buf);
				const b = new Uint8Array(t);
				if (a.length !== b.length) {
					console.log('unmatch length');
					throw 'BUG! UNMATCH';
				} else {
					for (let i = 0; i < a.length; i++) {
						if (a[i] !== b[i]) {
							console.log('unmatch');
							throw 'BUG! UNMATCH';
						}
					}
					console.log('matched!');
				}
			}
		}
	})
	.catch( err => {
		console.log(`failed: ${err}`);
	});
}

IDPWMemoTest();

document.querySelector('main > aside button.show')!
	.addEventListener('click', () => showMemo());

document.querySelector('main > article button.load')!
	.addEventListener('click', () => loadService());

document.querySelector('main > article button.value')!
	.addEventListener('click', () => addNewValue());

document.querySelector('main > article button.secret')!
	.addEventListener('click', () => addNewSecret());

document.querySelector('main > article button.service')!
	.addEventListener('click', () => addNewService());

document.querySelector('main > article button.download')!
	.addEventListener('click', () => downloadMemo());

