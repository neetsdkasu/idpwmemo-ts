/* Data IO Demo
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

function dataIODemo(): void {
	const ul = <HTMLElement>document.querySelector('ul');
	ul.innerHTML = '';
	const ts: number[] = [0, 1, 2, 3, 4, 5, 6, 7];
	for (let i = 7; i > 0; i--) {
		const j = Math.floor(Math.random()*(i+1));
		const t = ts[i];
		ts[i] = ts[j];
		ts[j] = t;
	}
	const ss: string[] = ['ABCD', 'xyz', '987', 'あいう', '漢字', '１２３', '○Π〈Å', '𝟘𝟙𝟚𝟛😃'];
	let s = '';
	for (let i = Math.floor(Math.random()*10)+6; i > 0; i--) {
		const x = [...ss[Math.floor(Math.random()*ss.length)]];
		s += x[Math.floor(Math.random()*x.length)];
	}
	let len = DataOutput.sizeHintWriteUTF(s);
	const buf = new ArrayBuffer(len+8+8+4+4+2+1+1);
	const dout = new DataOutput(buf);
	for (const t of ts) {
		let v = Math.random() - 0.5;
		switch (t) {
		case 0:
			dout.writeUTF(s);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeUTF("${s}")`;
			break;
		case 1:
			v *= (-10) ** Math.floor(v*80);
			dout.writeDouble(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeDouble(${v})`;
			break;
		case 2:
			const bg = BigInt(Math.floor(v*1e8)) ** 2n;
			dout.writeLong(bg);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeLong(${bg})`;
			break;
		case 3:
			v *= (-10) ** Math.floor(v*80);
			dout.writeFloat(v);
			const f = new Float32Array(1);
			f[0] = v;
			ul.appendChild(document.createElement('li'))
				.textContent = `writeFloat(${f[0]})`;
			break;
		case 4:
			v = Math.floor(v*1e8);
			dout.writeInt(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeInt(${v})`;
			break;
		case 5:
			v = Math.floor(v*1e4);
			dout.writeShort(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeShort(${v})`;
			break;
		case 6:
			v = Math.floor(v*250);
			dout.writeByte(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeByte(${v})`;
			break;
		case 7:
			dout.writeBoolean(v < 0);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeBoolean(${v < 0})`;
			break;
		}
	}
	const din = new DataInput(buf);
	let dst = '';
	let code = '';
	for (const t of ts) {
		code += '\t\tSystem.out.println(';
		switch (t) {
		case 0:
			code += '"UTF: " + dis.readUTF());\n';
			dst += `readUTF: "${din.readUTF()}"
`;
			break;
		case 1:
			code += '"Double: " + dis.readDouble());\n';
			dst += `readDouble: ${din.readDouble()}
`;
			break;
		case 2:
			code += '"Long: " + dis.readLong());\n';
			dst += `readLong: ${din.readLong()}
`;
			break;
		case 3:
			code += '"Float: " + dis.readFloat());\n';
			dst += `readFloat: ${din.readFloat()}
`;
			break;
		case 4:
			code += '"Int: " + dis.readInt());\n';
			dst += `readInt: ${din.readInt()}
`;
			break;
		case 5:
			code += '"Short: " + dis.readShort());\n';
			dst += `readShort: ${din.readShort()}
`;
			break;
		case 6:
			code += '"Byte: " + dis.readByte());\n';
			dst += `readByte: ${din.readByte()}
`;
			break;
		case 7:
			code += '"Boolean: " + dis.readBoolean());\n';
			dst += `readBoolean: ${din.readBoolean()}
`;
			break;
		}
	}
	const ub = new Uint8Array(buf);
	document.querySelector('output')!
		.textContent = `size: ${ub.length}
data: [${ub}]

${dst}`;
	document.querySelector('code')!
		.textContent = `import java.io.*;

class Program {
	public static void main(String[] args) throws Exception {
		byte[] buf = new byte[]{${new Int8Array(buf)}};
		InputStream bais = new ByteArrayInputStream(buf);
		DataInput dis = new DataInputStream(bais);
		${code.trim()}
	}
}
`;
}

function dataIOTest(): void {
	const b = new ArrayBuffer(13+8+4+8+4+2+2+1+1+1+1+14);

	const d = new DataOutput(b);

	d.writeUTF('あいうDx');
	d.writeDouble(-123.45);
	d.writeInt(100007);
	d.writeLong(100007n+(2n**35n)+(2n**41n)+(2n**44n));
	d.writeFloat(-98.765);
	d.writeShort(-20009);
	d.writeShort(65001);
	d.writeByte(-128);
	d.writeByte(128);
	d.writeBoolean(true);
	d.writeBoolean(false);
	d.writeBytes('SAMPLE\nsample\n');

	console.log(b);

	const r = new DataInput(b);

	console.log(r.readUTF());
	console.log(r.readDouble());
	console.log(r.readInt());
	console.log(r.readLong());
	console.log(r.readFloat());
	console.log(r.readShort());
	console.log(r.readUnsignedShort());
	console.log(r.readByte());
	console.log(r.readUnsignedByte());
	console.log(r.readBoolean());
	console.log(r.readBoolean());
	console.log(r.readLine());
	console.log(r.readLine());
}

dataIOTest();
dataIODemo();

document.querySelector('button')!
	.addEventListener('click', () => dataIODemo());
