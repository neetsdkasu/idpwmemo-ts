/* unkocyrpto Demo
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

class JavaRandom {

	private seed: bigint = 0n;

	public constructor(seed?: bigint) {
		this.setSeed(seed ?? BigInt(Math.floor(Math.random()*1e52)));
	}

	public setSeed(seed: bigint): void {
		this.seed = (seed ^ 0x5DEECE66Dn) & ((1n << 48n) - 1n);
	}

	protected next(bits: bigint): bigint {
		this.seed = (this.seed * 0x5DEECE66Dn + 0xBn) & ((1n << 48n) - 1n);
		return BigInt.asIntN(32, this.seed >> (48n - bits));
	}

	public nextInt(): number {
		return Number(this.next(32n));
	}
}

function showBytes(buf: ArrayBuffer): void {
	const b = new Uint8Array(buf);
	let s = '';
	for (let i = 0; i < b.length; i++) {
		if (b[i] < 0x10) {
			s += ` ${b[i].toString(16)}, `;
		} else {
			s += `${b[i].toString(16)}, `;
		}
		if (i % 10 === 9) {
			s += '\n';
		}
	}
	console.log(s);
}

function int8ArraysEauals(a: Int8Array, b: Int8Array): boolean {
	if (a.length !== b.length) {
		return false;
	}
	for (let i = 0; i < a.length; i++) {
		if (a[i] !== b[i]) {
			return false;
		}
	}
	return true;
}

function unkoCryptoTest(): void {
	const data = new Int8Array([1,2,3,4,5,6,7,8,9,10]);
	const encSrc = data.buffer;
	const seed = 123456789n;
	const rand = new JavaRandom();
	const cs = new CRC32();
	const blockSize = UnkoCrypto.MIN_BLOCKSIZE;

	console.log(`seed: ${seed}`);
	console.log(`blockSize: ${blockSize}`);
	console.log('data:');
	showBytes(encSrc);
	/* (stdout)
		seed: 123456789
		blockSize: 32
		data:
		 1,  2,  3,  4,  5,  6,  7,  8,  9,  a,
	*/

	const encDst = new ArrayBuffer(blockSize);
	rand.setSeed(seed);

	// Encrypt
	UnkoCrypto.encrypt(blockSize, cs, rand, encSrc, encDst);

	console.log('secret:');
	showBytes(encDst);
	/* (stdout)
		secret:
		a2, fc, 45, 90, 64, 80, 77, 46, 3f, 7e,
		1d, 7c, 64, fe, 5c, 98, 7a,  0, 79, a8,
		64, f2, 7d, c1, e3, 66, 31, 31, 1e, 62,
		b6,  4,
	*/

	const decSrc = encDst;
	const decDst = new ArrayBuffer(blockSize);
	rand.setSeed(seed);

	// Decrypt
	const len = UnkoCrypto.decrypt(blockSize, cs, rand, decSrc, decDst);

	const recover = new Int8Array(decDst.slice(0, len));
	console.log('recover:');
	showBytes(recover.buffer);
	/* (stdout)
		recover:
		 1,  2,  3,  4,  5,  6,  7,  8,  9,  a,
	*/

	if (int8ArraysEauals(data, recover)) {
		console.log('match!');
	}
	/* (stdout)
		match!
	*/
}

function getDemoRandom(password: string): JavaRandom {
	let seed = 0n;
	for (let i = 0; i < password.length; i++) {
		seed ^= BigInt(password.charCodeAt(i)) << BigInt((i % 6) << 3);
	}
	const rand = new JavaRandom(seed);
	for (let i = (seed % 10n) + 1n; i >= 0n; i--) {
		rand.nextInt();
	}
	return rand;
}

function encryptDemo(): void {
	const password = (<HTMLInputElement>document.querySelector('#enc input')).value;
	const text = (<HTMLTextAreaElement>document.querySelector('#enc textarea')).value;
	const rand = getDemoRandom(password);
	const blockSize = UnkoCrypto.MIN_BLOCKSIZE;
	const dataSize = blockSize - UnkoCrypto.META_SIZE;
	const src = (new TextEncoder()).encode(text);
	const blockCount = Math.ceil(src.length / dataSize);
	const dst = new ArrayBuffer(blockCount * blockSize);
	const len = UnkoCrypto.encrypt(blockSize, new CRC32(), rand, src.buffer, dst);
	document.querySelector('#enc output')!
		.textContent = `${new Uint8Array(dst.slice(0, len))}` + '\n\n';
}

function decryptDemo(): void {
	const password = (<HTMLInputElement>document.querySelector('#dec input')).value;
	const values = (<HTMLTextAreaElement>document.querySelector('#dec textarea')).value;
	const rand = getDemoRandom(password);
	const blockSize = UnkoCrypto.MIN_BLOCKSIZE;
	try {
		const src = new Uint8Array(values.split(',').map(v => Number(v)));
		const dst = new ArrayBuffer(src.length);
		const len = UnkoCrypto.decrypt(blockSize, new CRC32(), rand, src.buffer, dst);
		const text = (new TextDecoder()).decode(new Uint8Array(dst.slice(0, len)));
		document.querySelector('#dec output')!.textContent = text + '\n\n';
	} catch (ex) {
		document.querySelector('#dec output')!.textContent = '';
		if (ex instanceof UnkoCryptoException) {
			alert(`decrypt ERROR! (${ex.message})`);
		} else {
			throw ex;
		}
	}
}

function setDemoSample(): void {
	(<HTMLInputElement>document.querySelector('#enc input'))
		.value = 'Password1';
	(<HTMLTextAreaElement>document.querySelector('#enc textarea'))
		.value = 'サンプルSAMPLE';
	(<HTMLInputElement>document.querySelector('#dec input'))
		.value = 'Password1';
	(<HTMLTextAreaElement>document.querySelector('#dec textarea'))
		.value = '190,100,193,35,59,68,164,102,249,78,131,47,50,32,122,137,11,210,214,14,67,42,220,207,180,239,140,129,29,229,59,221';
}

unkoCryptoTest();
setDemoSample();

document.querySelector('#enc button')!
	.addEventListener('click', () => encryptDemo());

document.querySelector('#dec button')!
	.addEventListener('click', () => decryptDemo());
