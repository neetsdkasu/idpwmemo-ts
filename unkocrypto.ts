/* unkocrypto
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

// ArrayBufferLike, ArrayBufferView are defined in lib.es5.d.ts

interface Checksum {
	reset(): void;
	update(buf: number|ArrayBufferLike|ArrayBufferView): void;
	getValue(): bigint;
}

interface IntRNG {
	nextInt(): number; // 32bits Integer
}

class ByteReader {
	private readonly data: Uint8Array;
	private pos: number = 0;
	public constructor(buf: ArrayBufferLike|ArrayBufferView) {
		if (ArrayBuffer.isView(buf)) {
			this.data = new Uint8Array(
				buf.buffer,
				buf.byteOffset,
				buf.byteLength
			);
		} else {
			this.data = new Uint8Array(buf);
		}
	}
	public read(): number {
		if (this.pos >= this.data.length) {
			return -1;
		}
		return this.data[this.pos++];
	}
}

class UnkoCryptoException {
	public readonly message: string;
	public constructor(message: string) {
		this.message = message;
	}
}

class UnkoCrypto {

	public static readonly MIN_BLOCKSIZE: number = 32;
	public static readonly MAX_BLOCKSIZE: number = 1 << 20;
	public static readonly META_SIZE: number = (32/8) + (64/8);
	private static readonly BYTE: number = 0x100;
	private static readonly MASK: number = 0x0FF;

	// 0 < bound <= 2^31
	private static nextInt(rand: IntRNG, bound: number): number {
		if ((bound & (-bound)) === bound) {
			return Math.floor((rand.nextInt() >>> 1) / ((2**31)/bound));
		}
		let bits: number;
		let val: number;
		do {
			bits = rand.nextInt() >>> 1;
			val = bits % bound;
		} while (bits - val + (bound - 1) < 0);
		return val;
	}

	public static decrypt(
		blockSize: number,
		checksum: Checksum,
		rand: IntRNG,
		srcBuf: ArrayBufferLike|ArrayBufferView,
		dstBuf: ArrayBufferLike|ArrayBufferView
	): number {
		const MASK = UnkoCrypto.MASK;
		const BYTE = UnkoCrypto.BYTE;
		const nextInt = UnkoCrypto.nextInt;
		const src = new ByteReader(srcBuf);
		const dst = new DataOutput(dstBuf);
		const dataSize = blockSize - UnkoCrypto.META_SIZE;
		const mask = new Int32Array(blockSize);
		const indexes = new Int32Array(blockSize);
		const data = new Uint8Array(blockSize);
		const dis = new DataInput(data.buffer);
		let len = 0;
		let onebyte = src.read();
		if (onebyte < 0) {
			throw new UnkoCryptoException('TYPE_INVALID_DATASIZE');
		}
		while (onebyte >= 0) {
			for (let i = 0; i < blockSize; i++) {
				mask[i] = nextInt(rand, BYTE);
				indexes[i] = i;
			}
			for (let i = 0; i < blockSize; i++) {
				const j = nextInt(rand, blockSize - i) + i;
				const tmp = indexes[i];
				indexes[i] = indexes[j];
				indexes[j] = tmp;
			}
			for (let i = 0; i < blockSize; i++) {
				if (onebyte < 0) {
					throw new UnkoCryptoException('TYPE_INVALID_DATASIZE');
				}
				const j = indexes[i];
				let b = MASK & onebyte;
				b ^= MASK & mask[j];
				data[j] = b;
				onebyte = src.read();
			}
			dis.reset();
			dis.skipBytes(dataSize);
			const count = dis.readInt();
			const code = dis.readLong();
			if (count < 0 || (count === 0 && onebyte >= 0) || dataSize < count) {
				throw new UnkoCryptoException('TYPE_INVALID_COUNT');
			}
			len += count;
			checksum.reset();
			for (let i = 0; i < dataSize; i++) {
				if (i < count) {
					dst.writeByte(data[i]);
					checksum.update(MASK & data[i]);
				} else if (data[i] !== 0) {
					throw new UnkoCryptoException('TYPE_INVALID_DATA');
				}
			}
			if (code !== checksum.getValue()) {
				throw new UnkoCryptoException('TYPE_INVALID_CHECKSUM');
			}
		}
		return len;
	}

	public static encrypt(
		blockSize: number,
		checksum: Checksum,
		rand: IntRNG,
		srcBuf: ArrayBufferLike|ArrayBufferView,
		dstBuf: ArrayBufferLike|ArrayBufferView
	): number {
		const MASK = UnkoCrypto.MASK;
		const BYTE = UnkoCrypto.BYTE;
		const nextInt = UnkoCrypto.nextInt;
		const src = new ByteReader(srcBuf);
		const dst = new DataOutput(dstBuf);
		const dataSize = blockSize - UnkoCrypto.META_SIZE;
		const baos = new ArrayBuffer(blockSize);
		const memory = new Uint8Array(baos);
		const dos = new DataOutput(baos);
		let len = 0;
		let onebyte = src.read();
		do {
			len += blockSize;
			dos.reset();
			checksum.reset();
			let count = 0;
			for (; count < dataSize; count++) {
				if (onebyte < 0) {
					break;
				}
				let b = MASK & onebyte;
				checksum.update(b);
				b ^= MASK & nextInt(rand, BYTE);
				dos.writeByte(b);
				onebyte = src.read();
			}
			for (let i = count; i < dataSize; i++) {
				const b = MASK & nextInt(rand, BYTE);
				dos.writeByte(b);
			}
			dos.writeInt(count);
			dos.writeLong(checksum.getValue());
			for (let i = dataSize; i < memory.length; i++) {
				let b = MASK & memory[i];
				b ^= MASK & nextInt(rand, BYTE);
				memory[i] = b;
			}
			for (let i = 0; i < memory.length; i++) {
				const j = nextInt(rand, memory.length - i) + i;
				const tmp = memory[i];
				memory[i] = memory[j];
				memory[j] = tmp;
			}
			dst.write(memory.buffer);
		} while (onebyte >= 0);
		return len;
	}
}
