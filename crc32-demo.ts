/* CRC32 Demo
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

document.querySelector('button')!
.addEventListener('click', () => {
	const t = <HTMLTextAreaElement>document.querySelector('textarea');
	const blob = new Blob([t.value]);
	blob.arrayBuffer().then( buf => {
		const crc = new CRC32();
		crc.update(buf);
		document.querySelector('output')!.textContent = `${crc.getValue()}`;
	});
});
