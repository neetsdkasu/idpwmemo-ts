/* Mersenne Twister Demo
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

// 数値(整数)文字列整形
function formatInt(n: number, val: number): string {
    let t = val.toString();
    while (t.length < n) {
        t = ' ' + t;
    }
    return t;
}

// 数値(小数)文字列整形
function formatDbl(n: number, m: number, val: number): string {
    const k = (val + Math.pow(10, -m - 1) * 5.0).toString();
    let t = k.split('.');
    while (t[0].length < (n - m - 1)) {
        t[0] = ' ' + t[0];
    }
    while (t[1].length < m) {
        t[1] = t[1] + '0';
    }
    if (t[1].length > m) {
        t[1] = t[1].substring(0, m);
    }
    return (t[0] + '.' + t[1]).substring(0, n);
}

function mersenneTwiterDemo(): void {
	console.log('start demo');

	const seed = new Uint32Array([0x123, 0x234, 0x345, 0x456]);

	const mt = new MersenneTwister(seed);

	let text = '';

	text += '1000 outputs of genrand_int32()\n';
	for (let i=0; i<1000; i++) {
		text += formatInt(10, mt.genrand_int32()) + ' ';
		if ((i % 5) == 4) {
			text += '\n';
		}
	}

	text += '\n1000 outputs of genrand_real2()\n';
	for (let i=0; i<1000; i++) {
		text += formatDbl(10, 8, mt.genrand_real2()) + ' ';
		if ((i % 5) == 4) {
			text += '\n';
		}
	}

	(<HTMLTextAreaElement>document.querySelector('textarea'))
		.value = text;

	console.log('end demo');
}

mersenneTwiterDemo();
